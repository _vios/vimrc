colorscheme blue
set expandtab
set smarttab
"ハイライトサーチ
set hlsearch
"インクリメントサーチ
set incsearch
set autoindent
"set cindent
set smartindent
set sw =4
set tw=0
"行番号
set nu
"タブ、行末の表示
set list

"utf8で編集する
set fenc=utf8

"for neocomplcache
"neocomplcacheを有効にする
"let g:neocomplcache_enable_at_startup = 1
"imap <C-k> <Plug>(neocomplcache_snippets_expand)
"smap <C-k> <Plug>(neocomplcache_snippets_expand)

"ctfl-jをescにリマップする
imap <C-j> <C-[>

"print with num
:set printoptions=number:y

"'#'入力によるインデント解除を無効にする
autocmd FileType python :inoremap # X#

"ステータスラインに文字コードと改行コードを情報を表示する
 set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P
